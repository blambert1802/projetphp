<?php

include_once('dataTraitement.php');

class TextQuestion{

    protected $id;
    protected $intitule;
    protected $reponse;
    protected $categorie;
    protected $rightAnswer;
    public function __construct($id, $intitule, $reponse, $categorie){
        $this->id = (string) $id;
        $this->intitule = $intitule;
        $this->reponse = $reponse;
        $this->categorie = $categorie;
        $this->rightAnswer = false;
    }

    public function getId(){
        return $this->id;
    }

    public function getCategorie(){
        return $this->categorie;
    }

    public function getAnswer(){
        return $this->reponse;
    }

    /**
     * La fonction vérifie si la réponse donnée par l'utilisateur est correcte selon la réponse
     * de la classe. Elle return soit True, soit False selon si elle correspond ou non.
     * @param $answer
     * @return bool
     */
    public function isCorrect($answer){
        return strtolower($this->reponse) == strtolower($answer);
    }

    /**
     * @return string
     */
    public function toHtml(){

        $html = "<div class = 'text' >\n";
        $html.= "<label for='".$this->id."'>".$this->intitule."</label>\n";
        $html.= "<input type='text' id ='".$this->id."' name ='".$this->id."'>\n";
        $html.= "</div>";
        return $html;
        
    }

    /**
     * @return string|null
     */
    public function getPhpVariable(){
        if ( isset( $_GET[$this->id] ) && $_GET[$this->id] != ""){
            return make_valid($_GET[$this->id]);
        }
        return null;
    }

    /**
     * Prends en paramètre un score et l'incrémente de 1 si le résultat est bon.
     * La fonction adapte également l'affichage html selon si la réponse est bonne ou non.
     * @param $score
     * @return string
     */
    public function answerToQuestion($score){

        $html = "<div class = 'answer'>\n";
        $html.= "<p> La question était : ".$this->intitule."</p>\n";

        $answer = $this->getPhpVariable();

        if ( isset($answer) ){
            $html.= "<p> Vous avez répondu : ".$answer."</p>\n";

            if ( $this->isCorrect($answer) ){
                $html.= "<p class = 'true' > C'est une bonne réponse !</p>\n";
                $this->rightAnswer = true;
            }
            else {
                $html.= "<p class = 'false' > C'est une mauvaise réponse, la bonne réponse était : ".$this->reponse."</p>\n";
            }
        }
        else {
            $html.= "<p class = 'false' > Vous n'avez pas répondu à cette question. </p>\n";
        }

        $html.= $score->ajouter_score( (int) $this->rightAnswer, 1);

        $html.= "</div>\n";

        return $html;
    }
    
}

class RadioQuestion extends TextQuestion{

    private $choix;

    public function __construct($id, $intitule, $reponse, $choix, $categorie){
        parent::__construct($id, $intitule, $reponse, $categorie);
        $this->choix = $choix;
    }

    public function toHtml(){

        $html = "<div class = 'radio' >\n";
        $html.= "<label>".$this->intitule."</label>\n";
        
        foreach ($this->choix as $c){
            $html.= "<br>";
            $html.= "<input type='radio' id ='".$c."' name ='".$this->id."' value = '".$c."'>\n";
            $html.= "<label for='".$c."'>".$c."</label>\n";
        }

        $html.= "</div>";
        return $html;
    }

}

class CheckboxQuestion extends TextQuestion{

    private $choix;

    public function __construct($id, $intitule, $reponses, $choix, $categorie){
        parent::__construct($id, $intitule, $reponses, $categorie);
        $this->choix = $choix;
    }

    public function isCorrect($answers){
        return $this->reponse == $answers;
    }

    public function toHtml(){

        $html = "<div class = 'checkbox' >\n";
        $html.= "<label>".$this->intitule."</label>\n";
        
        foreach ($this->choix as $c){
            $html.= "<br>";
            $html.= "<input type='checkbox' id ='".$c.$this->id."' name ='".$c.$this->id."'>\n";
            $html.= "<label for='".$c.$this->id."'>".$c."</label>\n";
        }

        $html.= "</div>";
        return $html;
    }

    public function getChoix(){
        return $this->choix;
    }

    public function getPhpVariable(){
        $answers = array(); 

        foreach ( $this->choix as $choix ){
            if ( isset($_GET[$choix.$this->id]) ){
                array_push($answers, $choix);
            }

        }
        return $answers;
    }

    public function answerToQuestion($score){
        $html = "<div class = 'answer'>\n";
        $html.= "<p> La question était : ".$this->intitule."</p>\n";

        $answers = $this->getPhpVariable();

        if ( count($answers) != 0 ){
            $html.= "<p> Vous avez sélectionné : </p>\n";
            $html.= "<ul>\n";
            foreach($answers as $answer){
                $html.= "<li>".$answer."</li>\n";
            }
            $html.= "</ul>\n";

            if ( $this->isCorrect($answers) ){
                $html.= "<p class = 'true' > C'est une bonne réponse !</p>\n";
                $this->rightAnswer = true;
            }

            else {
                $html.= "<p class = 'false' > C'est une mauvaise réponse, la bonne réponse était : </p>\n";
                $html.= "<ul>\n";
                foreach ($this->reponse as $reponse){
                    $html.= "<li>".$reponse."</li>\n";
                }
                $html.= "</ul>\n";
            }
        }
        else {
            $html.= "<p class = 'false' > Vous n'avez rien sélectionné. </p>\n";
        }

        $html.= $score->ajouter_score( (int) $this->rightAnswer, 1);

        $html.= "</div>\n";
        return $html;
    }
}

class ListQuestion extends TextQuestion{

    private $choix;

    public function __construct($id, $intitule, $reponse, $choix, $categorie){
        parent::__construct($id, $intitule, $reponse, $categorie);
        $this->choix = $choix;
    }

    public function toHtml(){

        $html = "<div class = 'radio' >\n";
        $html.= "<label for='".$this->id."'>".$this->intitule."</label>\n";
        $html.= "<select name = '".$this->id."' id = '".$this->id."'>\n";
        foreach ($this->choix as $c){
            $html.= "<option value = '".$c."'>".$c."</option>";
        }
        $html.="</select>\n";
        $html.= "</div>";
        return $html;
    }
    
}


?>