# projetPHP

Baptiste LAMBERT
Guillaume VANDECASTEELE

Projet : Programmation Web côté serveur
Sujet : création d'un formulaire en ligne intéragisseant avec une base de données et avec de la POO

<details><summary>lancer le site</summary>



1. lancer le script de création de base de données :
- `php ./scriptBD.php`

Si vous utilisez une pile LAMP, ne faites pas la commande ci-dessus,
elle ne créera la BD qu'en local et sera non accessible par votre serveur.
ouvrez le fichier ./scriptBD.php dans votre navigateur,Ex : `http://localhost/scriptBD.php`

De base le fichier est créée dans /tmp/form.sqlite3 :
Si vous êtes sur WINDOWS ou que vous voulez simplement changer le PATH, changez le path dans la ligne `$file_db = new PDO('sqlite:/tmp/form.sqlite3');`

- Ex : `$file_db = new PDO('sqlite:/home/user/form.sqlite3');`

à changer dans les fichiers :
- scriptBD.php
- loginTraitement.php
- score.php
- questionFromBD.php


2. Une fois la BD insérée, vous pouvez vous rendre à la racine du projet :

Ex : 
- `http://localhost/projetPHP/`
- `http://localhost/`
- `http://localhost/projetPHP/index.html`

Voilà, vous pouvez désormais naviguer sur le site !

NB : pour des raisons de droit, la BD est créée dans /tmp/

</details>

<details><summary>Ajouter des questions</summary>


1. Pour ajouter des questions, allez dans `scriptBD.php` et rajouter dans l'`Array` > $row_questions les questions que vous souhaitez : les types sont :
- `TextQuestion` pour une question de type textuel
- `ListQuestion` pour une question a réponse unique avec une liste de choix
- `CheckboxQuestion` pour une question avec plusieurs réponses et plusieurs choix
- `RadioQuestion` pour une question a réponse unique avec plusieurs choix

Suivez la syntaxe des questions déjà ajoutées, vous êtes libres sur le nombre de choix, ou de réponse si le type inséré est le bon. Les catégories servent à différencier les questions lors de l'affichage, vous êtes donc libre de créer ce qu'il vous faut !

2. Ne pas oublier de relancer la BD par la suite en suivant le `1.` de la section `lancer le site`.
Si vous ne voulez pas supprimer les utilisateurs, commentez la ligne 125 (`//`).

> 1. lancer le script de création de base de données :
> - `php ./scriptBD.php`
> 
> Si vous utilisez une pile LAMP, ne faites pas la commande ci-dessus,
> elle ne créera la BD qu'en local et sera non accessible par votre serveur.
> ouvrez le fichier ./scriptBD.php dans votre navigateur,Ex : `http://localhost/scriptBD.php`

</details>

