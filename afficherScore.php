<?php

session_start();
include_once("score.php");

if (!$_SESSION["role"] == 'admin'){
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
</head>
<body>
    <h1>Affichage des Scores</h1>

    <a href="home.php">Page d'accueil</a>

    <?php
        echo Score::affiche_score($file_db);
    ?>

    
</body>
</html>