<?php

include_once('questions.php');

class QuestionFromBD{

    private $file_db;

    public function __construct(){
        $this->file_db = new PDO('sqlite:/tmp/form.sqlite3');
        $this->file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->questions = array();
        $this->categories = array();
        $this->createQuestion();
        $this->initializeCategories();
    }
    public function getQuestions(){
        return $this->questions;
    }

    public function getCategories(){
        return $this->categories;
    }

    public function reloadQuestions(){
        $this->questions = array();
        $this->createQuestion();
    }

    /**
     * Appelle toute les fonctions de création d'objet questions
     */
    function createQuestion(){
        $this->createTextQuestion();
        $this->createRadioQuestion();
        $this->createListQuestion();
        $this->createCheckboxQuestion();
    }

    /**
     * Créer à partir de la base de donnée l'objet question du type correspondant
     * et l'ajoute dans l'attribut questions de la classe
     */
    function createTextQuestion(){

        $resultSet = $this->file_db->query("select * from question natural join reponse where typeQ = 'TextQuestion'");

        foreach ($resultSet as $column){
            array_push($this->questions, new TextQuestion($column["idQ"], $column["intituleQ"], $column["reponseQ"], $column["categorieQ"]));
        }

    }

    /**
     * Créer à partir de la base de donnée l'objet question du type correspondant
     * et l'ajoute dans l'attribut questions de la classe
     */
    function createRadioQuestion(){

        $resultSet = $this->file_db->query("select * from question natural join reponse where typeQ = 'RadioQuestion'");

        foreach ($resultSet as $column){
            $id = $column["idQ"];
            $choix = array();
            $resultSetChoix = $this->file_db->query("select * from choix where idQ =".$id);
            
            foreach ($resultSetChoix as $c){
                array_push($choix, $c["intituleC"]);
            }

            array_push($this->questions, new RadioQuestion($column["idQ"], $column["intituleQ"], $column["reponseQ"], $choix, $column["categorieQ"]));
        }

    }

    /**
     * Créer à partir de la base de donnée l'objet question du type correspondant
     * et l'ajoute dans l'attribut questions de la classe
     */
    function createListQuestion(){

        $resultSet = $this->file_db->query("select * from question natural join reponse where typeQ = 'ListQuestion'");

        foreach ($resultSet as $column){
            $id = $column["idQ"];
            $choix = array();
            $resultSetChoix = $this->file_db->query("select * from choix where idQ =".$id);
            
            foreach ($resultSetChoix as $c){
                array_push($choix, $c["intituleC"]);
            }

            array_push($this->questions, new ListQuestion($column["idQ"], $column["intituleQ"], $column["reponseQ"], $choix, $column["categorieQ"]));
        }

    }

    /**
     * Créer à partir de la base de donnée l'objet question du type correspondant
     * et l'ajoute dans l'attribut questions de la classe
     */
    function createCheckboxQuestion(){

        $resultSet = $this->file_db->query("select * from question where typeQ = 'CheckboxQuestion'");

        foreach ($resultSet as $column){
            $id = $column["idQ"];
            $choix = array();
            $answers = array();

            $resultSetChoix = $this->file_db->query("select * from choix where idQ =".$id);
            
            foreach ($resultSetChoix as $c){
                array_push($choix, $c["intituleC"]);
            }

            $resultSetQuestion = $this->file_db->query("select * from reponse where idQ =".$id);

            foreach ($resultSetQuestion as $q){
                array_push($answers, $q["reponseQ"]);
            }

            array_push($this->questions, new CheckboxQuestion($column["idQ"], $column["intituleQ"], $answers, $choix, $column["categorieQ"]));
        }

    }

    /**
     * Ajoute les catégories à l'attribut categories de la classe
     */
    function initializeCategories(){
        $this->categories = array();
        foreach ($this->questions as $question){
            if (!in_array($question->getCategorie(), $this->categories)){
                array_push($this->categories, $question->getCategorie());
            }
        }
    }

}

?>