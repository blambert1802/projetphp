<?php

$row_questions = array(
    array(
        'type' => "TextQuestion",
        'intitule' => "Quelle est la capitale du Mexique ?",
        'reponse' => "Mexico",
        'categorie' => "Le jeu des Capitales"
    ),
    array(
        'type' => "TextQuestion",
        'intitule' => "Quelle est la capitale de l'Allemagne ?",
        'reponse' => "Berlin",
        'categorie' => "Le jeu des Capitales"
    ),
    array(
        'type' => "TextQuestion",
        'intitule' => "Quelle est la capitale de la Pologne ?",
        'reponse' => "Varsovie",
        'categorie' => "Le jeu des Capitales"
    ),
    array(
        'type' => "TextQuestion",
        'intitule' => "Quelle est la capitale du Danemark ?",
        'reponse' => "Copenhague",
        'categorie' => "Le jeu des Capitales"
    ),
    array(
        'type' => "RadioQuestion",
        'intitule' => "Combien de pays sont membres de l'UE ?",
        'reponse' => "27",
        'choix' => array(
            "22",
            "25",
            "27"
        ),
        'categorie' => "Question de Culture Générale"
    ),
    array(
        'type' => "CheckboxQuestion",
        'intitule' => "Parmi ces pays, lesquels sont membres de l'UE",
        'reponse' => array(
            "France",
            "Allemagne"
        ),
        'choix' => array(
            "France", 
            "Russie", 
            "Suisse", 
            "Royaume-Uni", 
            "Allemagne"
        ),
        'categorie' => "Question de Culture Générale"
    ),
    array(
        'type' => "RadioQuestion",
        'intitule' => "Quel est le nombre d'habitant en France ? (en millions)",
        'reponse' => "67",
        'choix' => array(
            "65", 
            "67", 
            "69",
            "71"
        ),
        'categorie' => "Question de Culture Générale"
    ),
    array(
        'type' => "CheckboxQuestion",
        'intitule' => "Parmi ces pays, lesquels font parti des trois premières puissances mondiales ?",
        'reponse' => array(
            "Etats-Unis",
            "Chine",
            "Japon"
        ),
        'choix' => array(
            "Etats-Unis", 
            "Allemagne", 
            "Chine", 
            "Inde", 
            "France",
            "Japon"
        ),
        'categorie' => "Question de Culture Générale"
    ),
    array(
        'type' => "ListQuestion",
        'intitule' => "Quel pays se situe en Afrique ?",
        'reponse' => "Angola",
        'choix' => array(
            "Paraguay",
            "Angola",
            "Chili", 
            "Mexique", 
            "Cuba",
            "Bangladesh"
        ),
        'categorie' => "Géographie et Espaces"
    ),
    array(
        'type' => "ListQuestion",
        'intitule' => "Parmi ces villes laquelle est la plus peuplée ?",
        'reponse' => "Tokyo",
        'choix' => array(
            "Jakarta", 
            "New Delhi", 
            "Shangaï", 
            "Paris", 
            "Tokyo",
            "New York"
        ),
        'categorie' => "Géographie et Espaces"
    )
);

try{
    
    $file_db = new PDO('sqlite:/tmp/form.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /**
     * Prends en paramètre le fichier sqlite et supprime toutes les tables de la base de données
     * @param $file_db
     */
    function dropTables($file_db){

        $file_db->exec("DROP TABLE IF EXISTS question");
        $file_db->exec("DROP TABLE IF EXISTS reponse");
        $file_db->exec("DROP TABLE IF EXISTS choix");
        $file_db->exec("DROP TABLE IF EXISTS user");
        $file_db->exec("DROP TABLE IF EXISTS score");
    }

    /**
     * Se produit après la fonction dropTables afin de réinitialiser la base de données
     * @param $file_db
     */
    function createTables($file_db){

        $file_db->exec("
            CREATE TABLE IF NOT EXISTS question (
            idQ INTEGER PRIMARY KEY AUTOINCREMENT,
            typeQ TEXT,
            intituleQ TEXT,
            categorieQ TEXT
            )"
        );

        $file_db->exec("
            CREATE TABLE IF NOT EXISTS reponse (
            idQ INTEGER,
            idR INTEGER PRIMARY KEY AUTOINCREMENT,
            reponseQ TEXT,
            FOREIGN KEY (idQ) REFERENCES question(idQ)
            )"
        );

        $file_db->exec("
            CREATE TABLE IF NOT EXISTS choix (
            idQ INTEGER,
            idC INTEGER PRIMARY KEY AUTOINCREMENT,
            intituleC TEXT,
            FOREIGN KEY (idQ) REFERENCES question(idQ)
            )"
        );

        $file_db->exec("
            CREATE TABLE IF NOT EXISTS user (
            pseudoU TEXT PRIMARY KEY,
            pwdU TEXT,
            roleU TEXT
            )"
        );

        $file_db->exec("
            CREATE TABLE IF NOT EXISTS score (
            idS INTEGER PRIMARY KEY AUTOINCREMENT,
            pseudoU TEXT,
            score INTEGER,
            scoreMax INTEGER,
            FOREIGN KEY (pseudoU) REFERENCES user(pseudoU)
            )"
        );
    }

    /**
     * Renvoie le dernier identifiant de la table question, par conséquent renvoie la dernière question créée
     * dans la table.
     * @param $file_db
     * @return int|mixed
     */
    function getMaxId($file_db){
        $stmt = $file_db->prepare("SELECT max(idQ) idQ FROM question");
        $stmt->execute();
        $row = $stmt->fetch();
        $id = $row['idQ'];
        if ($id == ""){
            return 1;
        } 
        return $id;
        
    }

    /**
     * Prends en paramètre la base de données ainsi que tout les éléments nécessaires pour créer une nouvelle question
     * de type textQuestion. Implémente ainsi la question dans la base de donnée.
     * @param $file_db
     * @param $typeQ
     * @param $intituleQ
     * @param $categorieQ
     * @param $reponse
     */
    function creaTextQuestion($file_db, $typeQ, $intituleQ, $categorieQ, $reponse){

        // Insererer la question dans la base de donnees
        $request = "INSERT INTO question (typeQ, intituleQ, categorieQ) VALUES (:typeQ, :intituleQ, :categorieQ)";
        $stmtQ = $file_db->prepare($request);
        $stmtQ->bindParam(':typeQ', $typeQ);
        $stmtQ->bindParam(':intituleQ', $intituleQ);
        $stmtQ->bindParam(':categorieQ', $categorieQ);
        $stmtQ->execute();

        // Inserer la reponse dans la base de donnees
        $result = GetMaxId($file_db);
        $requestR = "INSERT INTO reponse (idQ, reponseQ) VALUES (:idQ, :reponseQ)";
        $stmtR = $file_db->prepare($requestR);
        $stmtR->bindParam(':idQ', $result);
        $stmtR->bindParam(':reponseQ', $reponse);
        $stmtR->execute();
    }

    /**
     * Prends en paramètre la base de données ainsi que tout les éléments nécessaires pour créer une nouvelle question
     * de type radioQuestion ou de type listQuestion. Implémente ainsi la question dans la base de donnée.
     * @param $file_db
     * @param $typeQ
     * @param $intituleQ
     * @param $categorieQ
     * @param $reponse
     * @param $choix
     */
    function creaRadioOrListQuestion($file_db, $typeQ, $intituleQ, $categorieQ, $reponse, $choix){

        // Insererer la question dans la base de donnees
        $request = "INSERT INTO question (typeQ, intituleQ, categorieQ) VALUES (:typeQ, :intituleQ, :categorieQ)";
        $stmtQ = $file_db->prepare($request);
        $stmtQ->bindParam(':typeQ', $typeQ);
        $stmtQ->bindParam(':intituleQ', $intituleQ);
        $stmtQ->bindParam(':categorieQ', $categorieQ);
        $stmtQ->execute();

        // Inserer la reponse dans la base de donnees
        $result = GetMaxId($file_db);
        $requestR = "INSERT INTO reponse (idQ, reponseQ) VALUES (:idQ, :reponseQ)";
        
        $stmtR = $file_db->prepare($requestR);
        $stmtR->bindParam(':idQ', $result);
        $stmtR->bindParam(':reponseQ', $reponse);
        $stmtR->execute();

        // Inserer les choix dans la base de donnees
        foreach($choix as $elem){

            $requestC = "INSERT INTO choix (idQ, intituleC) VALUES (:idQ, :intituleC)";
            $stmtC = $file_db->prepare($requestC);
            $stmtC->bindParam(':idQ', $result);
            $stmtC->bindParam(':intituleC', $elem);
            $stmtC->execute();

        }
    }

    /**
     * Prends en paramètre la base de données ainsi que tout les éléments nécessaires pour créer une nouvelle question
     * de type checkboxQuestion. Implémente ainsi la question dans la base de donnée.
     * @param $file_db
     * @param $typeQ
     * @param $intituleQ
     * @param $categorieQ
     * @param $reponses
     * @param $choix
     */
    function creaCheckboxQuestion($file_db, $typeQ, $intituleQ, $categorieQ, $reponses, $choix){

        // Inserser la question dans la base de donnes
        $request = "INSERT INTO question (typeQ, intituleQ, categorieQ) VALUES (:typeQ, :intituleQ, :categorieQ)";
        $stmtQ = $file_db->prepare($request);
        $stmtQ->bindParam(':typeQ', $typeQ);
        $stmtQ->bindParam(':categorieQ', $categorieQ);
        $stmtQ->bindParam(':intituleQ', $intituleQ);

        $stmtQ->execute();

        // Inserer les reponses dans la base de donnees
        $result = GetMaxId($file_db);
        foreach($reponses as $reponse){

            $requestR = "INSERT INTO reponse (idQ, reponseQ) VALUES (:idQ, :reponseQ)";
            $stmtR = $file_db->prepare($requestR);
            $stmtR->bindParam(':idQ', $result);
            $stmtR->bindParam(':reponseQ', $reponse);
            $stmtR->execute();

        }

        // Inserer les choix dans la base de donnees
        foreach($choix as $elem){

            $requestC = "INSERT INTO choix (idQ, intituleC) VALUES (:idQ, :intituleC)";
            $stmtC = $file_db->prepare($requestC);
            $stmtC->bindParam(':idQ', $result);
            $stmtC->bindParam(':intituleC', $elem);
            $stmtC->execute();

        }
    }

    /**
     * Insère les questions dans la base de données
     * @param $file_db
     * @param $row_questions
     */
    function insertQuestion($file_db, $row_questions){
        foreach($row_questions as $question){
            if ($question["type"] == "TextQuestion"){
                creaTextQuestion($file_db, $question["type"], $question["intitule"], $question["categorie"], $question['reponse']);
            }
            elseif ($question["type"] == "RadioQuestion" or $question["type"] == "ListQuestion"){
                creaRadioOrListQuestion($file_db, $question["type"], $question["intitule"], $question["categorie"], $question['reponse'], $question['choix']);
            }
            elseif ($question["type"] == "CheckboxQuestion"){
                creaCheckboxQuestion($file_db, $question["type"], $question["intitule"], $question["categorie"], $question['reponse'], $question['choix'] );
            }
        }
    }

    /**
     * Supprime la question renseignée de la base de donnée
     * @param $file_db
     * @param $idQ
     */
    function deleteQuestion($file_db, $idQ){
        $request = "delete from choix where idq=".$idQ;
        $file_db->exec($request);

        $request = "delete from reponse where idq=".$idQ;
        $file_db->exec($request);

        $request = "delete from question where idq=".$idQ;
        $file_db->exec($request);
    }
    
    dropTables($file_db);
    createTables($file_db);
    insertQuestion($file_db, $row_questions);
    
    $st = $file_db->prepare("INSERT INTO user (pseudoU, pwdU, roleU) values ('admin',:pass, 'admin')");
    $pwd =  hash("sha512", 'admin');
    $st->bindParam(":pass", $pwd);
    $st->execute();

    

    echo "creation bd";


}catch (PDOException $e) {
    echo $e->getMessage()."\n";
}
