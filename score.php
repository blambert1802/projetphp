<?php

try{

    $file_db = new PDO('sqlite:/tmp/form.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    echo $e->getMessage()."\n";
}

/**
 *
 */
class Score{

    /**
     * @var int
     */
    private $score;

    /**
     *
     */
    public function __construct(){
        $this->score = 0;
        $this->score_max = 0;

    }

    /**
     * @return int
     */
    public function getScore(){
        return $this->score;
    }

    /**
     * @return int
     */
    public function getMax(){
        return $this->score_max;
    }


    /**
     * Actualise les attributs de Score avec les points passés en paramètres
     * @param $points
     * @param $points_max
     * @return string
     */
    public function ajouter_score($points, $points_max){

        $this->score += $points;
        $this->score_max += $points_max;

        return "<p class = 'score' > Vous gagnez ".$points."/".$points_max." point. </p>\n";
    }

    /**
     * Stock le score contenu dans l'instance en base de données
     * @param $file_db
     * @param $pseudoU
     */
    public function stock_score($file_db, $pseudoU){
        try{
            $file_db->exec("INSERT INTO score (pseudoU, score, scoreMax) VALUES ('".$pseudoU."', '".$this->score."', '".$this->score_max."')");
        } catch (PDOException $e) {
            echo $e->getMessage()."\n";
        }
    }

    /**
     * Retourne le code html corresondant à l'affichage des scores de tous les scores stockés en BD
     * @param $file_db
     * @return string|void
     */
    public static function affiche_score($file_db){

        $html = "";

        try{
            $rs = $file_db->query("select * from score order by idS");

            foreach ($rs as $r){
                $html.= "<p>";
                $html.= $r["pseudoU"]." a marqué ".$r["score"]." points sur ".$r["scoreMax"]. " points";
                $html.= "</p>\n";
            }
        
            return $html;

        }catch (PDOException $e) {
            echo $e->getMessage()."\n";
        }
    }

}

?>