<?php
/** 
 * vérifie l'intégrité de la donnée récupérée par le formulaire
 * retourne la donnée "nettoyée"
 * @param $donnees
 * @return string
**/
function make_valid($donnees){
    $donnees = trim($donnees); // supprime les caractères inutiles
    $donnees = stripslashes($donnees); // supprimer les antislashes 
    $donnees = htmlspecialchars($donnees); // remplacer les caractères HTML
    return $donnees;
}

?>