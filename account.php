<?php

session_start();
include_once('dataTraitement.php');
include_once('loginTraitement.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Créer un compte</title>
</head>
<body>
    <h1>Inscription</h1>

    <a href="home.php">Page d'acceuil</a>

    <?php

    if ($_POST['submit']){
        $pseudo = make_valid($_POST['pseudo']);
        $pwd = make_valid($_POST['pwd']);
        $pwd2 = make_valid($_POST['pwd2']);

        if ($pwd == $pwd2){

            if (!userExist($file_db, $pseudo)){

                createUser($file_db, $pseudo, $pwd);
                $_SESSION["pseudo"] = $pseudo;
                $_SESSION["role"] = 'user';
                header("Location: index.php");

            } else { echo "<p>Ce pseudo est déjà pris</p>\n"; }

        } else { echo "<p>Les mots de passe ne correspondent pas</p>\n"; }
    }

    ?>

    <form method="POST">

    <fieldset>

        <legend>Créer un compte</legend>

        <p>
            <label for="pseudo">Pseudo: </label>
            <input type="text" id="pseudo" name="pseudo" placeholder="Venom" required>
        </p>

        <p>
            <label for="pwd">Mot de passe: </label>
            <input type="password" id="pwd" name="pwd" placeholder="*********" required>
        </p>

        <p>
            <label for="pwd2">Confirmation Mot de passe: </label>
            <input type="password" id="pwd2" name="pwd2" placeholder="*********" required>

        </p>


        <input type="submit" name = 'submit' value="Créer un compte"/>

    </fieldset>

    </form>

    <p>Déjà inscrit ? <a href="login.php">Connectez vous.</a></p>
    
</body>