<?php

// on démarre une session

session_start();

include_once('questionFromBD.php');
include_once('score.php');

try{
    $qb = new QuestionFromBD($file_db);
}catch (PDOException $e) {
    echo $e->getMessage()."\n";
}
$questions = $qb->getQuestions();
$categories = $qb->getCategories();
$score = new Score();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Quizz de Géographie</title>
</head>
<body>

    <header>
        <h1>Questionnaire de Géographie</h1>
    </header>

    <?php

        if ($_GET['submit']){

            echo "<a href='home.php'>Page d'accueil</a><br>";
            if (isset($_SESSION["pseudo"])){
                echo "<a href='deco.php'>Se déconnecter</a><br>";
            }

            foreach ($categories as $c){
                echo "<fieldset class = '".$c."'>\n";
                echo "<legend>".$c."</legend>\n";
        
                foreach ($questions as $q){
                    if ($q->getCategorie() == $c){
                        echo $q->answerToQuestion($score)."<br>";
                    }
                }
                echo "</fieldset><br>\n";
            }
        
            echo "<h2> Votre score est de ".$score->getScore()."/".$score->getMax()." points ! </h2>";

            if (isset($_SESSION["pseudo"])){
                $score->stock_score($file_db, $_SESSION["pseudo"]);
            }

        }

        else{

            if (isset($_SESSION["pseudo"])){
                echo "Content de vous revoir ".$_SESSION["pseudo"]." : "."<a href='deco.php'>Se déconnecter</a><br>";

                if ($_SESSION["role"] == "admin"){
                    echo "<a href='afficherScore.php'>Voir les scores</a>";
                }
            }
    
            else {
                echo "<a href='login.php'>Se connecter</a>";
            }

            echo "<form method='GET'>\n";

            foreach ($categories as $c){
                echo "<fieldset class = '".$c."'>\n";
                echo "<legend>".$c."</legend>\n";
        
                foreach ($questions as $q){
                    if ($q->getCategorie() == $c){
                        echo $q->toHtml()."<br>";
                    }
                }
                echo "</fieldset>\n";
            }

            echo "<input type='submit' name = 'submit' value='Envoyer' />\n";
            echo "</form>\n";

        }

    ?>
    
</body>
</html>