<?php

session_start();
include_once('dataTraitement.php');
include_once('loginTraitement.php');

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
</head>
<body>
    
    <h1>Connexion</h1>

    <a href="home.php">Page d'accueil</a>

    <?php

        if ($_POST['submit']){

            $pseudo = make_valid($_POST["pseudo"]);
            $pwd = make_valid($_POST["pwd"]);
            if (verifyConnexion($file_db, $pseudo, $pwd)){
                
                $_SESSION["pseudo"] = $pseudo;
                $_SESSION["role"] = getRole($file_db, $pseudo);

                header("Location: index.php");

            } else {
                echo "<p>Pseudo ou mot de passe incorrect</p>";
            }
        }

    ?>

    <form method="POST">

    <fieldset>

        <legend>Connexion</legend>

        <p>
            <label for="pseudo">Pseudo: </label>
            <input type="text" id="pseudo" name="pseudo" placeholder="Venom" required>
        </p>

        <p>
            <label for="pwd">Mot de passe: </label>
            <input type="password" id="pwd" name="pwd" placeholder="*********" required>
        </p>


        <input type="submit" name = "submit" value="Se connecter"/>

    </fieldset>

    </form>

    <p>Pas encore inscrit ? <a href="account.php">Créer un compte.</a></p>
    
</body>
</html>