<?php

try{
    $file_db = new PDO('sqlite:/tmp/form.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    echo $e->getMessage()."\n";
}
/**
 * Vérifie dans la base de données rentrée en paramètre si le couple pseudo / mot de passe existe.
 * Return le booléen correspondant
 * @param $file_db
 * @param $pseudoU
 * @param $pwdU
 * @return int|mixed|void
 */
function verifyConnexion($file_db, $pseudoU, $pwdU){
    try{
        
        $pwdU = hash("sha512", $pwdU);

        $stmt = $file_db->prepare("select count(*) bool from user where pseudoU = :pseudoU and pwdU = :pwdU ");
        $stmt->bindParam(':pseudoU', $pseudoU);
        $stmt->bindParam(':pwdU', $pwdU);
        $stmt->execute();


        foreach ($stmt->fetchAll() as $r){
            return $r["bool"];
        }
        return 0;

    }catch (PDOException $e) {
        echo $e->getMessage()."\n";
    }

}

/**
 * Vérifie dans la base de données rentrée en paramètre si le pseudo existe.
 * Return le booléen correspondant
 * @param $file_db
 * @param $pseudoU
 * @return int|mixed|void
 */
function userExist($file_db, $pseudoU){
    try{
        $rs = $file_db->query("select count(*) bool from user where pseudoU = '".$pseudoU."'");
        foreach ($rs as $r){
            return $r["bool"];
        }
        return 0;
    }catch (PDOException $e) {
        echo $e->getMessage()."\n";
    }
}

/**
 * Ajoute un utilisateur et son mot de passe dans la base de donnée
 * @param $file_db
 * @param $pseudoU
 * @param $pwdU
 */
function createUser($file_db, $pseudoU, $pwdU){
    try{
        $pwdU = hash("sha512", $pwdU);
        $request = "INSERT INTO user (pseudoU, pwdU, roleU) values ( :pseudoU, :pwdU, 'user')";

        $stmt = $file_db->prepare($request);
        $stmt->bindParam(':pseudoU', $pseudoU);
        $stmt->bindParam(':pwdU', $pwdU);
        $stmt->execute();

    }catch (PDOException $e) {
        echo $e->getMessage()."\n";
    }
}

/**
 * Renvoie le rôle d'un utilisateur selon la base de donnée
 * @param $file_db
 * @param $pseudoU
 * @return mixed|void
 */
function getRole($file_db, $pseudoU){
    try{
        $rs = $file_db->query("select roleU from user where pseudoU = '".$pseudoU."'");
        foreach ($rs as $r){
            return $r["roleU"];
        }
    }catch (PDOException $e) {
        echo $e->getMessage()."\n";
    }
}

?>